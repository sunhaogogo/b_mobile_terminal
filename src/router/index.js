import Vue from 'vue'
import Router from 'vue-router'
import store from '../store'
Vue.use(Router)
const game = (resolve) => {
  import('components/game').then((module) => {
    resolve(module)
  })
}
const gift = (resolve) => {
  import('components/gift').then((module) => {
    resolve(module)
  })
}
const activity = (resolve) => {
  import('components/activity').then((module) => {
    resolve(module)
  })
}
const manage = (resolve) => {
  import('components/manage').then((module) => {
    resolve(module)
  })
}
const login = (resolve) => {
  import('components/login').then((module) => {
    resolve(module)
  })
}
const Desc = (resolve) => {
  import('components/Desc').then((module) => {
    resolve(module)
  })
}
const activityDesc = (resolve) => {
  import('components/activityDesc').then((module) => {
    resolve(module)
  })
}
const search = (resolve) => {
  import('components/search').then((module) => {
    resolve(module)
  })
}
const service = (resolve) => {
  import('components/service').then((module) => {
    resolve(module)
  })
}
const router=new Router({
  routes: [
    {
      path: '/',
      redirect:'/game'
    },
    {
      path:'/game',
      component:game,
      children:[
        {
            path:'/game/:id',
             component: Desc
         },
      ]
    },
    {
      path:'/manage',
      component: manage,
      children:[
        {
            path:'/manage/login',
            component: login,
         },
      ]
    },
    {
      path:'/gift',
      component:gift
    },
    {
      path:'/activity',
      component:activity,
      children:[
        {
          path:'/activity/:id',
          component: activityDesc,
       },
      ]
    },
    {
      path:'/search',
      component:search
    },
    {
      path:'/service',
      component:service
    }
  ]
})
const mainRouteList = ['game', 'gift', 'activity', 'manage'];
router.beforeEach((to, from, next) => {
  const toPath =to.path;
  console.log(toPath)
  const fromPath = from.path;
  const toIndex = mainRouteList.indexOf(toPath.slice(1));
  const fromIndex = mainRouteList.indexOf(fromPath.slice(1));
  if (toIndex - fromIndex > 0) {
      store.commit('UPDATE_DIRECTION', 'forward');
  } else if (toIndex - fromIndex < 0) {
      store.commit('UPDATE_DIRECTION', 'reverse');
  }
  next();
})

export default router;