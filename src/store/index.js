import Vue from 'vue'
import Vuex from 'vuex'
import * as getters from './getter'
import actions from './actions' 

import state from './store'
import mutations from './mutation'

Vue.use(Vuex)


export default new Vuex.Store({
    getters,
    actions,
    mutations,
    state,
})
