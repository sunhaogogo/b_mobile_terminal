import * as types from './mutations-types'
const mutations={
    [types.SET_NICKNAME](state,nickName){
        sessionStorage.setItem('nickName', JSON.stringify(nickName))
         state.nickName= Object.assign(state.nickName, nickName);
         //这个地方用到es6对象的浅拷贝
    },
    [types.UPDATE_DIRECTION](state, direction) {
        state.direction = direction;
    }
}

export default mutations;